# README #

This project was developed by Jorge Hidalgo on April 27th, 2018

### REQUIREMENTS ###

The project completed the following requirements

* Login of registered users
* List of enterprises
* Enterprises' details
* Filter by name and type

# RUNNING THE PROJECT #

* Download all the files, including the node_modules in order to work which I think it is a considerable practice in industrial app development. 

* Run npm install.

* Run npm start.

* The project will be running in localhost:9000


# PROJECT STRUCTURE #

* css, contains the single file that styles the project

* libs, contains the bootstrap library

* src, divided in /pages and /components, the first one contains the class component that describes the view, the last one contains all the functional components.

* imgs, contains the images of the project

### FUNCTIONAL DECISIONS ###

* I decided to skip the second screen in the Layout provided at Zeplin, in this case based on the fact that this particular view did not provide a functionality itself, it is a screen to guide the user to search enterprises, something that can be made with the next view. I thought it was not necessary to have two different screens for this matter, and simply open with the search view. 

* Also, the search view automatically makes a call for all the enterprises and saves them, making the other calls provided at postman unnecessary. In case that this network operation results in lots of data, it would be necessary to compare the time of programmatically filtering all the enterprises versus calls searching for specific enterprises.

* The filter functions looking for a match of type or name of the query written, so it runs two filters and adds the results to a javascript set. 

* I used webpack to get the project going, also attempted to use Sass but I had issues adding it with npm. Nevertheless, I used BEM syntax for the most part, in order to describe the elements' classes. 

### DESIGN DECISIONS ###

* Bootstrap v3 was used in this case, the bootstrap lib is saved in the lib folder. The integration I used with Bootstrap allowed to use glyphicons instead of the logos provided at Zeplin, which fit better in terms of responsiveness. The app is contained with a bootstrap container, and also class such as alert and navbar where used. 

* I put some colors in the enterprises images/logo just for visual purposes. 

* Media queries, there are a group of media queries to fit the requirement of responsiveness, they manipulate several HTML elements in order to make UI nicer.  

* Other elements were used, such as divs that appear with information in the login page in case the user and password is incorrect, and at the search page to inform about a network operation or as a catch for a network error. 

