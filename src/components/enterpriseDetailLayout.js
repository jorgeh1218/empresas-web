/*
*********************************
	Developed by Jorge Hidalgo 	*
	Date 27/04/2018				*
	iOasys						*
*********************************
*/

import React from 'react';
import SearchBar from "./searchBar";

/*EnterpriseDetailLayout
  Renders the detailed view of the enterprise when clicked in the search page
*/
function EnterpriseDetailLayout(props)
{
	let myStyle = {backgroundColor: props.color};
	return(
			<div className="enterpDetailLayout">

					<div className="enterpDetailLayout__imgContainer" style={myStyle}>
						<h1 className="enterpDetailLayout__imgContainer-letter">E{props.item.id}</h1>
					</div>
					<p className="enterpDetailLayout__text">{props.item.description}</p>

			</div>
	);

}

export default EnterpriseDetailLayout;