/*
*********************************
	Developed by Jorge Hidalgo 	*
	Date 27/04/2018				*
	iOasys						*
*********************************
*/

import React from 'react';

/*EnterpriseItem
  Renders the structure that describes every enterprise in the search page
*/
function EnterpriseItem(props)
{	
	let index;
	if(props.id % 2 == 0)
		index = 0;
	else if(props.id % 3 == 0)
		index = 1;
	else
		index = 2;

	let myStyle = {backgroundColor: props.color[index]};

	return(

		<div key={props.id} className="enterpItem"
			 onClick={(event) => props.handleClickEnterp(props.id, index, event)}>

			<div className="enterpItem__imgContainer" style={myStyle}>
				<h1 className="enterpItem__imgContainer-letter">E{props.id}</h1>
			</div>
			<div className="enterpItem__infoContainer">
				<h3 className="enterpItem__name">{props.name}</h3>
				<h3 className="enterpItem__business">{props.business}</h3>
				<h3 className="enterpItem__country">{props.country}</h3>
			</div>

		</div>
	);

}

export default EnterpriseItem;