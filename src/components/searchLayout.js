/*
*********************************
	Developed by Jorge Hidalgo 	*
	Date 27/04/2018				*
	iOasys						*
*********************************
*/

import React from 'react';
import SearchBar from "./searchBar";

/*SearchLayout
  Renders the complete layout that holds the search bar
*/
function SearchLayout(props)
{
	return(
		<div className="searchLayout">

			<SearchBar changeSearchParam={props.changeSearchParam}/>
					
		</div>
	);

}

export default SearchLayout;