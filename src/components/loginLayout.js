/*
*********************************
	Developed by Jorge Hidalgo 	*
	Date 27/04/2018				*
	iOasys						*
*********************************
*/

import React from 'react';

/*LoginLayout
  Renders the complete layout of the login screen
*/

function LoginLayout(props)
{
	return(
		<div className="loginLayout">
				<img className="img-responsive loginLayout__logo" 	 srcSet="../../../imgs/logo-home.png 285w, 
				../../../imgs/logo-home@2x.png 570w,  ../../../imgs/logo-home@3x.png 855w"/>

				<h2 className="loginLayout__welcome">BEM-VINDO AO EMPRESAS</h2>
				<h2 className="loginLayout__message">Lorem ipsum dolor sit amet, 
				   contetur adipiscing elit. Nunc accumsan.</h2>

				<form className="loginLayout__form" onSubmit={props.handleSubmit}>

						<input className="loginLayout__user warning" placeholder="E-mail"
						onChange={props.handleUserChange} type="text"/><br/>
					
					<input className="loginLayout__password" placeholder="Senha"
							onChange={props.handlePasswordChange} type="password" /><br/>

					<input className="loginLayout__button btn" value="ENTRAR" type="submit"/> 
				</form>
		</div>
		
	);

}

export default LoginLayout;