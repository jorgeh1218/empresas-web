/*
*********************************
	Developed by Jorge Hidalgo 	*
	Date 27/04/2018				*
	iOasys						*
*********************************
*/

import React from 'react';
import EnterpriseItem from "../components/enterpriseItem";

/*EnterpriseListLayout
  Renders the list of enterprises displayed in the search page
*/
function EnterpriseListLayout(props)
{
	
	return(
		<div className="enterpriseListLayout">
			{
				props.enterprises.map((item)=> {
					return <EnterpriseItem key={item.id} id={item.id} 
										   color={props.color}
										   name={item.enterprise_name} 
										   country={item.country} 
										   business={item.enterprise_type.enterprise_type_name}
										   handleClickEnterp={props.handleClickEnterp}/>
				})
			}			
		</div>
	);

}

export default EnterpriseListLayout;


