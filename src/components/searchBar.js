/*
*********************************
	Developed by Jorge Hidalgo 	*
	Date 27/04/2018				*
	iOasys						*
*********************************
*/

import React from 'react';

/*SearchBar
  Renders the search bar 
*/
function SearchBar(props)
{
	return(
		<div className="searchBarContainer">
			<nav className="navbar">
			  <div className="container-fluid">
			      	<div className="form-group inner-addon left-addon">
			          <span className="glyphicon glyphicon-search"></span>
			        	
			        </div>
			        <input className="searchBarContainer__input" type="text" placeholder="Pesquisar"
			          			 onChange={props.changeSearchParam} name="search"/>

			    </div>
			</nav>
		</div>
	);

}

export default SearchBar;

