/*
*********************************
	Developed by Jorge Hidalgo 	*
	Date 27/04/2018				*
	iOasys						*
*********************************
*/

import React from 'react';
import ReactDOM from 'react-dom';
import EnterpriseDetailLayout from "../components/enterpriseDetailLayout";
import SearchLayout from "../components/searchLayout";

class EnterprisePage extends React.Component
{
	
	constructor(props)
	{
		super(props);
		try
		{
			this.state = {
			item: this.props.location.state.selected_enterprise[0], //Parameters passed in the router
			color: this.props.location.state.color //Color of the image item
			}
		}
		catch(error)
		{
			this.props.history.push("/");
		}
	}
	changeSearchParam = (event) => {
		let searching = event.target.value;
		this.props.history.goBack(); //If search go back
	}
	render()
	{	
		if(this.state != null)
			return(
				<div className="enterpriseContainerLayout">
					<SearchLayout changeSearchParam={this.changeSearchParam}/>
					<EnterpriseDetailLayout color={this.state.color} item={this.state.item}/>
				</div>
			);
		else
		{
			this.props.history.push("/");
			return null;
		}
	}
}

export default EnterprisePage;