/*
*********************************
	Developed by Jorge Hidalgo 	*
	Date 27/04/2018				*
	iOasys						*
*********************************
*/

import React from 'react';
import ReactDOM from 'react-dom';
import SearchLayout from "../components/searchLayout";
import axios from "axios";
import { Redirect } from 'react-router-dom';
import EnterpriseListLayout from "../components/enterpriseListLayout";
import jsonQuery from 'json-query';

class SearchPage extends React.Component
{
	constructor(props)
	{
		super(props);
		//Try to access router parameters, in case the user hits the address of this page, without login
		//this will trigger an error
		try
		{
			this.state ={
				enterprises: [],//All the enterprises
				showed_enterprises: [],//Actually shown enterprises in the screen
				client:this.props.location.state.client,
				uid:this.props.location.state.uid,
				accessToken:this.props.location.state.accessToken,
				searchParam: "",//search query
				alertHiding:true, //Alert of network error
				infoHiding:false, //Info of network operation taken place
				color:["#98e550","#4cb5f7","#e27a5d"] //Colors defined for the enterprises
			}
		}	
		catch(error)
		{
			this.props.history.push("/");
		}
	}
	/*handleClickEnterp
	parameters: i, color, event 
	description: It is triggered when a user clicks on an enterprise*/
	handleClickEnterp = (i, color, event) => {
		
		let query = null;
		query = this.state.enterprises.filter( function(item){return (item.id == i);});
		
		if(query)//Sending the color in order for the enterprise to render
			this.props.history.push({pathname: 'enterprise', state:{selected_enterprise: query, 
																	color:this.state.color[color]}});
	}

	/*changeSearchParam
	parameters: event
	description: It is triggered every time the user presses keys in the search field*/
	changeSearchParam = (event) => {

		let results = new Set();//Using a set to combine filters
		let results_array = null;
		let searching = event.target.value;

		//Filtering by name and type
		let queryType = this.state.enterprises.filter( function(item){return (item.enterprise_type.id == searching);} );
		let queryName = this.state.enterprises.filter( function(item){return (item.enterprise_name.toLowerCase().indexOf(searching) != -1);} );

		//Combining results
		if(queryName.length != 0)
			queryName.map((item)=>{
				results.add(item);
		});
		if(queryType.length != 0)
			queryType.map((item)=>{
				results.add(item);
		});

		results_array = Array.from(results);

		if(searching == "")
			this.setState({showed_enterprises: this.state.enterprises});  
		else
		  this.setState({showed_enterprises: results_array}) 
	}
	componentDidMount = () =>
	{
		try //Trying to access parameters from router
		{

			this.setState({infoHiding: false});
			this.setState({alertHiding : true});

			axios.get("http://54.94.179.135:8090/api/v1/enterprises", {headers: {uid:this.state.uid, 
			 client:this.state.client,"access-token":this.state.accessToken}})
			.then((response)=> { console.log(response)
				if(response.statusText=="OK" && response.status == 200)
				{
					this.setState({alertHiding : true});
					this.setState({infoHiding: true});
					this.setState({enterprises: response.data.enterprises});
					this.setState({showed_enterprises: response.data.enterprises})
				}
			 })
			.catch(error => {
								console.log(error)
								this.setState({infoHiding: true});
								this.setState({alertHiding : false});
							});
		}
		catch(error)
		{
			this.props.history.push("/");	
		}
	}
	render()
	{	
		//Conditional added for matters of error handling in case the user attempts to jump into this direction
		if(this.state != null) 
		{
			const warning = this.state.alertHiding; //Show Warning Alert
			const info = this.state.infoHiding; //Show Info Alert
			
			return(
				<div>
					<SearchLayout changeSearchParam={this.changeSearchParam}/>
					
					{!info ? (<div className="alert alert-info alertDefaultText">
	  		  			<p className="alertDefaultText__text">Looking for those enterprises, hold on</p>
	 		 		</div>) : (<div></div>)}
					
					{!warning ? (
					<div className="alert alert-danger alertTextSearch">
	  		  			<p className="alertTextSearch__text">Oops, there was a <strong>network problem</strong></p>
	 		 		</div>) : (<div></div>)}

					<EnterpriseListLayout color={this.state.color} enterprises={this.state.showed_enterprises} 
										  handleClickEnterp={this.handleClickEnterp}/>
				</div>
			);
		}	
		else
		{	
			this.props.history.push("/");
			return null;	
		}	
	}
}

export default SearchPage;