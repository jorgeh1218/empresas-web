/*
*********************************
	Developed by Jorge Hidalgo 	*
	Date 27/04/2018				*
	iOasys						*
*********************************
*/

import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import LoginLayout from '../components/loginLayout';

class LoginPage extends React.Component
{	
	state = {email: "", password:"", accessToken: "",
			 client: "", uid: "", alertHiding: true}; //Alert Hiding, it hides an alert for bad password

	/*handleUserChange
	parameters: event
	description: It is triggered everytime the user presses keys in the username field for login*/
	handleUserChange = event =>{
		this.setState({email: event.target.value});
	}

	/*handlePasswordChange
	parameters: event
	description: It is triggered everytime the user presses keys in the password field for login*/
	handlePasswordChange = event => {
		this.setState({password: event.target.value});
	}

	/*handleSubmit
	parameters: event
	description: It is triggered when the user clicks the enter buttom to login */
	handleSubmit = event =>{

		event.preventDefault();

		this.setState({alertHiding : true});//We hide the alert in case it was showing 

		axios.post("http://54.94.179.135:8090/api/v1/users/auth/sign_in",
			 {email:this.state.email, password:this.state.password})
			.then((response) => { 
				if(response.statusText=="OK" && response.status == 200){
					
					this.setState({alertHiding : true});
					this.setState({uid: response.headers["uid"]});
					this.setState({accessToken: response.headers["access-token"]});
					this.setState({client: response.headers["client"]});
					
					console.log(this.state);
					this.props.history.push({pathname: 'search', 
											state: {accessToken: this.state.accessToken, 
					  					  	client: this.state.client, 
					  		  			  	uid:this.state.uid}
					})//We go to search page
				}
			 })
			.catch(error => {
				console.log(error);
				this.setState({alertHiding : false}); //Show alert
			});
	}
	render()
	{
		const warning = this.state.alertHiding;//Conditional rendering of the alert
		return(
			<div>
				{!warning? (
					<div className="alert alert-danger">
	  		  			<p className="alertTextLogin">Oops, Maybe you typed a <strong>wrong user and password</strong> or there was a <strong>network problem</strong></p>
	 		 		</div>) : (<div></div>)}

				<LoginLayout alertHiding={this.state.alertHiding} handleSubmit={this.handleSubmit} 
							 handlePasswordChange={this.handlePasswordChange}
							 handleUserChange={this.handleUserChange}/>
			</div>
		);		
	}
}

export default LoginPage;

/*
http://54.94.179.135:8090/api/v1/users/auth/sign_in

Header
Content-Type: application/json

Body
{
  "email" : "testeapple@ioasys.com.br",
  "password" : "12341234"
}

*/

/*
<LoginLayout loginClick={this.loginClick} handlePasswordChange={this.handlePasswordChange} 
				handleUserChange={this.handleUserChange} handleSubmit={this.handleSubmit}/>
*/