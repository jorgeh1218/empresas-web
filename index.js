/*
*********************************
	Developed by Jorge Hidalgo 	*
	Date 27/04/2018				*
	iOasys						*
*********************************
*/

import React from 'react';
import ReactDOM from 'react-dom';
import LoginPage from "./src/pages/loginPage";
import SearchPage from "./src/pages/searchPage";
import EnterprisePage from "./src/pages/enterprisePage";
import { Switch, Redirect, Router, Route } from 'react-router-dom';
import createBrowserHistory from 'history/createBrowserHistory';
import Bootstrap from "bootstrap";

const history = createBrowserHistory();

const app = document.getElementById("app");

ReactDOM.render(
	<Router history={history}>
		<Switch>
			<Route path="/" exact component={LoginPage}/>
			<Route path="/search" exact component={SearchPage}/>
			<Route path="/enterprise" exact component={EnterprisePage}/>
			<Route path="*"  component={LoginPage}/>
		</Switch>
	</Router>, 
app);
